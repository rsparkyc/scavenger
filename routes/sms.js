var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');

var moment = require('moment-timezone');

/* GET users listing. */
router.all('/', function(req, res) {
    var now = new Date();
    console.log('got new sms message: ' + JSON.stringify(req.body));

    var mongoUrl = process.env['MONGOHQ_URL'];

    mongodb.MongoClient.connect(mongoUrl, function(err, db) {
        if (err) {
            console.error('Error:' + err);
            return BuildSMSMessage(res, 'Error connecting to mongo: ' + err);
        }

        var textBody = req.body.Body.toLowerCase();

        db.collection('keywords').findOne({_id:textBody}, function(err, item){
            if (err) {
                console.error('Error:' + err);
                return BuildSMSMessage(res, 'Error retrieving item from mongo: ' + err);
            }
            if(!item) {
                return BuildSMSMessage(res, 'Quit guessing and go find the clue!');
            }
            if (item.deliveryDate && item.deliveryDate > new Date()) {
                return BuildSMSMessage(res, 'You must wait until ' + moment(item.deliveryDate).tz('America/New_York').format('MM/DD/YYYY h:mm A') + ' before getting this clue');
            }

            return BuildSMSMessage(res, item.body, item.media);
        });
    });
});

function BuildSMSMessage(res, body, media) {
    res.setHeader('Content-Type', 'text/xml');
    var msg = {body: body};
    if (media) {
        console.log('Media: ' + media);
        msg.media = media;
    }

    return res.render('sms', msg);
}

module.exports = router;
